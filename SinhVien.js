//import DBconnection from './DBconnection.js';
var db = require('./DBConnection.js');
//const db = DBconnection;

function getUsers(request, response) {
	db.query('SELECT * FROM sinhvien ORDER BY id ASC', (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
}
  
  const getUserById = (request, response) => {
    const id = parseInt(request.params.id)
  
	db.query('SELECT * FROM sinhvien WHERE id = $1', [id], (error, results) => {
	  if (error) {
		throw error
	  }
	  response.status(200).json(results.rows)
	})
  }
  
  const createUser = (request, response) => {
	const { id, name, lop, dob } = request.body
  
	db.query('INSERT INTO sinhvien (id, name, class, dob) VALUES ($1, $2, $3, $4)', [id, name, lop, dob], (error, results) => {
	  if (error) {
		throw error
	  }
	  //var json = JSON.parse(results);
	  //console.log("Postgres response:",  results.rows[0] );
	  response.status(201).send(`User added with ID: ${id}`)
	})
  }
  
  function updateUser(request, response) {
	const { name, classid, dob, id } = request.body;
	db.query(
		'UPDATE sinhvien SET name = $1, class = $2, dob = $3 WHERE id = $4',
		[name, classid, dob, id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`User modified with ID: ${id}`);
		}
	);
  }
  function updateNameUser(request, response) {
	console.log('req.params.id', request.params.id);
	const id = parseInt(request.params.id)
	const {name} = request.body;
	db.query(
		'UPDATE sinhvien SET name = $1 WHERE id = $2',
		[name, id],
		(error, results) => {
			if (error) {
				throw error;
			}
			
			response.status(200).send(`User modified with ID: ${id}`);
		}
	);
  }
  
  const deleteUser = (request, response) => {
	const id = parseInt(request.params.id)
  
	db.query('DELETE FROM sinhvien WHERE id = $1', [id], (error, results) => {
	  if (error) {
		throw error
	  }
	  response.status(200).send(`User deleted with ID: ${id}`)
	})
  }
  
  module.exports = {
	getUsers,
	getUserById,
	createUser,
	updateUser,
	updateNameUser,
	deleteUser,
  }
