//import aexpress from 'express';
//import abodyparser from 'body-parser';
//import SinhVien from './SinhVien.js'
//const express = aexpress;

//const bodyparser = abodyparser;
var express = require('express');
var bodyparser = require('body-parser');
//var connection = require('./DBconnection.js');
//var routes = require('./routes.js');
//const db = SinhVien;
var db = require('./SinhVien.js');
var app = express();
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
//app.use("/",routes); 
app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/users', db.getUsers)
app.get('/users/:id', db.getUserById)
app.post('/users', db.createUser)
app.put('/updateNameUser/:id', db.updateNameUser)
app.delete('/users/:id', db.deleteUser)
var server = app.listen(3000, function() {
  console.log('Server listening on port ' + server.address().port);
});
module.exports = app;
